module.exports = {
  "sourceMaps": true,
  "comments": false,
  "presets": [
    [
      "@babel/env",
      {
        "modules": false,
        "targets": {
          "browsers": [
            "last 2 chrome versions",
            "last 2 firefox versions",
            "last 2 opera versions",
            "last 2 edge versions"
          ]
        }
      }
    ],
    "@babel/react",
    "@babel/typescript"
  ],
  "plugins": [
    ["@babel/plugin-proposal-decorators", { "legacy": true }],
    ["@babel/proposal-class-properties", { "loose": true }],
    "@babel/proposal-object-rest-spread",
    "@babel/plugin-transform-modules-commonjs",
    "@babel/plugin-transform-runtime",
    "@babel/plugin-syntax-dynamic-import"
  ]
};

