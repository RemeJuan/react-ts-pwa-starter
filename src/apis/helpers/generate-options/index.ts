/* eslint-disable import/prefer-default-export */

/**
 * Configure default options for API Headers
 * @param {string} token OIDC Token
 * @param {*} head Headers object additions/overrides
 * @param {string} [method=GET] HTTP method, default === GET
 * @param {*} body Body object
 */
export default (token?, head = {}, method = 'GET', body = {}) => {
  const doNotAllowBody = ['GET'];
  const methodCheck = method && doNotAllowBody.some(met => method !== met);
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: `Bearer ${token}`,
    ...head,
  };
  const opt = { headers, method };
  if (methodCheck) return { ...opt, body: JSON.stringify(body) };
  return opt;
};
