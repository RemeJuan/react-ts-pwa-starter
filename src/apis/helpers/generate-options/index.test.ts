import generateOptions from './';

describe('API Helpers: generateOptions', () => {
  const token = 'FAKE_TOKEN';

  it('default', () => {
    const expected = {
      headers: {
        Accept: 'application/json',
        Authorization: 'Bearer undefined',
        'Content-Type': 'application/json',
      },
      method: 'GET',
    };

    expect(
      generateOptions(),
    ).toEqual(expected);
  });

  it('with token', () => {
    const expected = {
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      method: 'GET',
    };

    expect(
      generateOptions(token),
    ).toEqual(expected);
  });

  it('with additional headers', () => {
    const headers = { 'x-no-cache': '' };
    const expected = {
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
        ...headers,
      },
      method: 'GET',
    };

    expect(
      generateOptions(token, headers),
    ).toEqual(expected);
  });

  it('with replaced headers', () => {
    const headers = { Accept: 'application/xml' };
    const expected = {
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
        ...headers,
      },
      method: 'GET',
    };

    expect(
      generateOptions(token, headers),
    ).toEqual(expected);
  });

  it('with alternate method', () => {
    const headers = {};
    const method = 'POST';

    const expected = {
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: '{}',
    };

    expect(
      generateOptions(token, headers, method),
    ).toEqual(expected);
  });

  it('with defined body content', () => {
    const headers = {};
    const method = 'POST';
    const body = { red: true };

    const expected = {
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify(body),
    };

    expect(
      generateOptions(token, headers, method, body),
    ).toEqual(expected);
  });
});
