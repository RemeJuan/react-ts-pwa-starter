export const { API_BASE_PATH } = process.env;

export const SEARCH_API_PATH = `${API_BASE_PATH}/search`;
