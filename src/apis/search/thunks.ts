import { SEARCH_API_PATH } from '~/apis/config';
import { generateOptions } from '~/apis/helpers';
import {
  searchPostStart,
  searchPostSuccess,
  searchPostError,
} from './actions';

const URL = `${SEARCH_API_PATH}`;

/**
 * The search endpoint for Header Search Bar Panel Results
 * @param {*} searchQuery: the search query
 */
export function search({
  searchQuery,
}) {
  return async (dispatch) => {
    const timestamp = Date.now();
    const startData = { searchQuery, timestamp };
    dispatch(searchPostStart(startData));
    try {
      const token = '';
      const url = `${URL}?searchQuery=${searchQuery}`;
      const body = {};

      const options = generateOptions(token, {}, 'POST', body);
      const response = await fetch(url, options);

      const json = await response.json();
      const success = { searchResults: json, timestamp };

      return dispatch(searchPostSuccess(success));
    } catch (error) {
      const err = { error: true, errorMessage: error };
      return dispatch(
        searchPostError({ ...err, timestamp }));
    }
  };
}
