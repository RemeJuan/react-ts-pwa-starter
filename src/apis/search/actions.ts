export const SEARCH_POST_START = 'api/search/SEARCH_POST_START';
export const SEARCH_POST_SUCCESS = 'api/search/SEARCH_POST_SUCCESS';
export const SEARCH_POST_ERROR = 'api/search/SEARCH_POST_ERROR';

export const searchPostStart = data => ({
  type: SEARCH_POST_START, data,
});

export const searchPostSuccess = data => ({
  type: SEARCH_POST_SUCCESS, data,
});

export const searchPostError = data => ({
  type: SEARCH_POST_ERROR, data,
});
