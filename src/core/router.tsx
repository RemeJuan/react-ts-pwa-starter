import React from 'react';
import {
  Route, Switch, Redirect,
} from 'react-router-dom';

import { Dashboard } from '~/dashboard';

const HOME = '/';

export default (
  <Switch>
    <Route
      exact
      path={HOME}
      component={Dashboard}
    />

    <Redirect from="/" to={HOME} />
  </Switch>
);
