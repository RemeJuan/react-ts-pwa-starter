import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';

import { PersistGate } from 'redux-persist/integration/react';
import { initialize, addTranslationForLanguage } from 'react-localize-redux';

// @ts-ignore
import enUK from '~/locale/en-UK.json';

import { locales } from './config';

import App from './app';
import routes from './router';

import configureStore from './store';

const { persistor, store } = configureStore();

interface IProps {
  store: object;
  history: any;
}

export default class Root extends Component<IProps> {
  componentWillMount() {
    store.dispatch(initialize(locales, { defaultLanguage: 'en-UK' }));
    store.dispatch(addTranslationForLanguage(enUK, 'en-UK'));
  }

  render() {
    const { history } = this.props;

    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <ConnectedRouter history={history}>
              <App>
                {routes}
              </App>
          </ConnectedRouter>
        </PersistGate>
      </Provider>
    );
  }
}
