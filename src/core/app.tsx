import React, {FunctionComponent, useState} from 'react';
import {IChildren} from '~/shared/interfaces';
import {ThemeProvider} from 'styled-components';

const App: FunctionComponent<IChildren> = (props) => {
  const [theme] = useState<object>({});
  const {children} = props;

  return (
    <ThemeProvider theme={theme}>
      {children}
    </ThemeProvider>
  );
};

export default App;
