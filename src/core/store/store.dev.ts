import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import {createBrowserHistory} from 'history';
import {composeWithDevTools} from 'redux-devtools-extension';
import {createLogger} from 'redux-logger';

import {persistReducer, persistStore} from 'redux-persist';

import {persistConfig} from '~/core/config';

import rootReducer from '../reducer';

const logger = createLogger({
  level: 'info',
  collapsed: true,
});

export const history = createBrowserHistory();

// Configure options for redux devtools below
const options = {};
const composeEnhancers = composeWithDevTools(options);
// @ts-ignore
const persistedReducer = persistReducer(persistConfig, rootReducer);

export default () => {
  const store = createStore(
    persistedReducer,
    composeEnhancers(
      applyMiddleware(thunk, logger)
    )
  );
  const persistor = persistStore(store);

  return {store, persistor};
};
