import {applyMiddleware, createStore} from 'redux';
import {createBrowserHistory} from 'history';
import thunk from 'redux-thunk';

import {persistReducer, persistStore} from 'redux-persist';
import {persistConfig} from '~/core/config';

import rootReducer from '../reducer';

export const history = createBrowserHistory();
// @ts-ignore
const persistedReducer = persistReducer(persistConfig, rootReducer);

export default () => {
  const store = createStore(
    persistedReducer,
    applyMiddleware(thunk)
  );
  const persistor = persistStore(store);

  return {store, persistor};
};
