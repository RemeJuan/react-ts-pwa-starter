import createEncryptor from 'redux-persist-transform-encrypt';

import storage from 'redux-persist/lib/storage';

export const encryptor = createEncryptor({
  secretKey: 'dummy-app',
  onError: () => {
    localStorage.clear();
    window.location.reload();
  },
});


export const persistConfig = {
  key: 'root',
  storage,
  blacklist: [],
  transforms: [encryptor],
  throttle: 1000,
};

export const locales = [
  { name: 'English UK', code: 'en-UK' },
  { name: 'English US', code: 'en-US' },
];
