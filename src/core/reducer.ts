import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import { localeReducer as locale } from 'react-localize-redux';

import logSlowReducers from 'redux-log-slow-reducers';

import { moleculesReducer } from '~/molecules';
import { organismReducers } from '~/organisms';

const reducers = {
  ...moleculesReducer,
  ...organismReducers,
  routing,
  locale,
};

const loggingReducers = logSlowReducers(reducers);

const appReducer = combineReducers(loggingReducers);

const rootReducer = (state, action) => appReducer(
  (action.type === 'RESET_APPLICATION' ? undefined : state), action);

export default rootReducer;
