export { generateOptions } from '~/apis/helpers';

function debounce(func, wait, immediate, ...args) {
  let timeout;
  return () => {
    const context = this;
    const later = () => {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

export const manageSelectedItems = (id, enabled, set, dispatcher) =>
  async (dispatch) => {
    if (!enabled) set.delete(id);
    else set.add(id);
    return dispatch(dispatcher(set));
  };

const shallowEqual = (objA, objB) => {
  if (objA === objB) return true;
  if (typeof objA !== 'object' || objA === null ||
    typeof objB !== 'object' || objB === null) return false;

  const keysA = Object.keys(objA);
  const keysB = Object.keys(objB);

  if (keysA.length !== keysB.length) return false;

  // Test for A's keys different from B.
  const bHasOwnProperty = Object.hasOwnProperty.bind(objB);
  for (let i = 0; i < keysA.length; i += 1) { // tslint:disable-line
    if (!bHasOwnProperty(keysA[i]) ||
      objA[keysA[i]] !== objB[keysA[i]]) return false;
  }

  return true;
};

export const shallowCompare = (instance, nextProps, nextState) =>
  !shallowEqual(instance.props, nextProps) ||
  !shallowEqual(instance.state, nextState);

const debounced = (dispatch, func, time, ...args) => (
  (debounce as any)(() => dispatch(func(...args)), time)
);

export const asyncDebounce = (func, time, ...args) => (
  dispatch => debounced(dispatch, func, time, ...args)()
);
