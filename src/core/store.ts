import storeProd, {history as historyProd} from './store/store.prod';
import storeDev, {history as historyDev} from './store/store.dev';

const {NODE_ENV} = process.env;
const exp = NODE_ENV === 'production' ? storeProd : storeDev;
export const history = NODE_ENV === 'production' ? historyProd : historyDev;

export default exp;
