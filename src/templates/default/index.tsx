import React, {Fragment, FunctionComponent} from 'react';

import {Content, Wrapper} from './styles';

interface IProps {
  children: JSX.Element;
  features: object;
  loading: boolean;
}

const DefaultTemplate: FunctionComponent<IProps> = (props) => {
  const {
    children,
    features,
    loading,
    ...rest
  } = props;

  return (
    <Fragment>
      {loading && <div>Loading...</div>}
      {!loading &&
      <Wrapper {...rest}>
        <Content data-qa="template-content">{children}</Content>
      </Wrapper>
      }
    </Fragment>
  );
};

DefaultTemplate.defaultProps = {
  features: undefined,
  loading: true,
};

export default DefaultTemplate;
