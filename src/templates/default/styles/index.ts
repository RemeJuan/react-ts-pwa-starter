import styled from 'styled-components';
import {size} from 'styled-theme';

import withProps from 'styled-components-ts';

export const Wrapper = withProps<any>(styled.div)`
  flex-direction: column;
  box-sizing: border-box;
  position: relative;
  overflow: hidden;
  width: 100%;
  height: 100%;
`;

export const Content = styled.main`
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  max-width: ${size('maxWidth')};
  flex-direction: column;
`;
