export interface IChildren {
  children: JSX.Element[] | JSX.Element | Element[] | Element;
}

export interface IHTMLInputEvent extends Event {
  target: HTMLInputElement & EventTarget;
}

// interface IFeature {
//   [key: string]: string;
// }

// export interface IFeatures {
//   [key: string]: IFeature;
// }

export interface IHistory {
  push: (route: string) => any;
}

// interface IContextBarItemsViews {
//   name: string;
// }

// export interface IContextBarItems {
//   backgroundColor: string;
//   color: string;
//   context: string;
//   tenant: string;
//   workspace: string;
//   team: string;
//   views: IContextBarItemsViews[];
// }
