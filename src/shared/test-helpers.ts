// @ts-ignore
import translations from '~/locale/en-UK.json';

export const locale = {
  languages: [
    {
      name: 'English UK',
      code: 'en-UK',
      active: true,
    },
  ],
  translations,
};
