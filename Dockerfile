FROM node:10.8-alpine

RUN mkdir -p /app
WORKDIR /app

COPY ./dist ./dist
COPY ./express.js ./express.js
COPY ./package.json ./package.json

RUN npm install express express-static-gzip

EXPOSE 5000 80
CMD ["node", "express"]