import { Selector } from 'testcafe';
import nock from 'nock';

import fillingCabinetMocks from './mock-data/filling-cabinet.json';
import menuButtonMocks from './mock-data/menu-button.json';
import chartMocks from './mock-data/charts-data';

// Filling cabinet mocks
nock('https://iplatform-node-mock-api.herokuapp.com')
  .log(console.log)
  .defaultReplyHeaders({
    'Access-Control-Allow-Origin': '*',
  })
  .get('/dashboard/left-nav')
  .reply(200, fillingCabinetMocks);

// Top Nav Mocks
nock('https://iplatform-node-mock-api.herokuapp.com')
  .log(console.log)
  .defaultReplyHeaders({
    'Access-Control-Allow-Origin': '*',
  })
  .get('/dashboard/top-nav')
  .reply(200, menuButtonMocks);

// Chart Data Mocks
nock('https://iplatform-node-mock-api.herokuapp.com')
  .log(console.log)
  .defaultReplyHeaders({
    'Access-Control-Allow-Origin': '*',
  })
  .get('/data-views/sales/300')
  .reply(200, chartMocks);

fixture`My Work Test`
  .page`http://localhost:8080/work-desk/`;

test('Account Filter Search', async (t) => {
  const header = await Selector('header');
  const menuIcon = await header.child('div').child('aside');
  const menu = await menuIcon.child('div');

  const SearchField = await menu.child('div');
  const FilterSearch = 'Page Viewer';

  const PageViewer = await Selector('a')
    .withAttribute('data-qa', 'main-menu-dropdown-content-inner')

  const PageViewerMainWrapper = await Selector('div')
    .withAttribute('data-qa', 'main-content-wrapper')

  const pageViewerTitle = await PageViewerMainWrapper.find('div');

  const bicycleInsurancePerson = await pageViewerTitle.find('div').nth(2);

  const EditDocButton = await Selector('button')
    .withAttribute('data-qa', 'context-bar-actions-item-button')

  const FirstName = await Selector('div')
    .withAttribute('data-qa', 'form-block-item').nth(1);
  const EnterName = 'Oswin Test Brownbag';
  const TickNameButton = await FirstName.find('svg');

  const SurName = await Selector('div')
    .withAttribute('data-qa', 'form-block-item').nth(2);
  const EnterSurname = 'Losper Test Brownbag';
  const TickSurnameButton = await SurName.find('svg');

  const CloseDocButton = await Selector('button')
    .withAttribute('data-qa', 'context-bar-actions-item-button').nth(1);

  await t
    .click(menuIcon)
    .typeText(SearchField, FilterSearch)
    .expect(menu.count).eql(1);

  await t
    .expect(menu.innerText).eql('\n\nPage Viewer\n');

  await t
    .click(PageViewer)
    .click(bicycleInsurancePerson);

  await t
    .click(EditDocButton);

  await t
    .click(FirstName)
    .typeText(FirstName, EnterName);

  await t
    .click(TickNameButton)

  await t
    .click(SurName)
    .typeText(SurName, EnterSurname);

  await t
    .click(TickSurnameButton);

  await t
    .click(EditDocButton)
     .wait(2e3);

  await t
    .click(CloseDocButton);
});
