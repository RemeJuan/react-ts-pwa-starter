const PUBLIC_PATH = '/';
const themeColor = '#5aa7c7';
const bgColor = '#8dd8f8';
const favicon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAIAAAGLH901AAAACXBIWXMAABYlAAAWJQFJUiTwAAAC1UlEQVRIx2PU19VjgAEmBgaGuNWnGBgYHNJKGNFl3IqaGBgYWDk4MGRwc8QUlCDaUPToufpc2r0FixaIqKC4JLoEBJhGZ6IbhQxwSiCMUrd2EFNQQpdQNDS9efTAqwf3oubuQEjoufrcP38awl+W7KFoaErIDkjYwYFJQARUYlGoGVwubvWpMxtWkOUPXIAFjQ+x69HuVe/u31DzjOSSVb0wqwke4ig2xK0+dbgtG+5PNFPWprp9/fCBHCcxMZAImJBTBzwekIGguCRyAKM7iVtAwL93BQufEIR7Z8PsY0tnUxSstPc0C6aQmIKShKrWz29fbh49QMAGPVefuNWn7PKaGRgYJLVN4lafQkuAKH5wSCuRMndZluyBLK1oaGpbNXVRqBm6DdwCAnKuYWiqGRgY7p8/fbgtG9keZglxcQYGBhlN3UsrZ/34+gXT0R9ePPv/4fnLe7foFazkajAJiICUVlgBsqehGs5sWCFh6QEpr9BA1NwdF2Y1YXHS8lg73ykbke0RU1CKW33qzdVT2LMoBFhFp6oEpELYfz6921gcAcmZdEytJNtA83ijQn5DK0L0vUMVnILhpQgc/Pn07sG+tRe3rkYLdaKCiJWDwzGrSsLSg4GB4cXxHRfWLXj14B5mvjcISoCr2T+t7fePH0RZoOfqY5BW9+fTu+1Vie9fPicYCILikp5t81n4hNAqBZTsjFyJ6SZWvDi+Y0NlMtbcjQl+fP1yedMScVlZlYBk1t+fn924MrxSEc0tQI8DRUNTOV2jX18/ExkByFGtZuXAxc//4cUzfD54df+2QVqd75SNkEYOkUDR0NR3ykaDtLpX928TlQ8c0krkXMP+fHp3fEot1iYHstGWOc0sfEKPdq86MKuHtLIIkiEg7G+Pb7+5cf7d/RsMDAxCihoiGoZcsqoQKazJn+RkKqagJCgtx87Fw8DA8PPbl/dPH2Hm7WFamgIAszca2asOv4cAAAAASUVORK5CYII=';

module.exports = {
  title: 'Dummy Application',
  PUBLIC_PATH,
  themeColor,
  preCache: {
    cacheId: 'website-cache-id',
    dontCacheBustUrlsMatching: /\.\w{8}\./,
    filename: 'service-worker.js',
    minify: true,
    navigateFallback: `${PUBLIC_PATH}index.html`,
    staticFileGlobsIgnorePatterns: [/\.map$/, /asset-manifest\.json$/],
  },
  manifest: {
    name: 'Dummy',
    short_name: 'Dummy', // 12 Characters max.
    description: 'Stuff goes here',
    background_color: bgColor,
    theme_color: themeColor,
    'theme-color': themeColor,
    start_url: PUBLIC_PATH,
    // icons: [
    //   {
    //     src: path.resolve('src/core/assets/images/PWA_Icon.jpg'),
    //     sizes: [96, 128, 192, 256, 384, 512],
    //     destination: path.join('assets', 'icons'),
    //   },
    // ],
  },
  favicon,
};
