const webpackMerge = require('webpack-merge');
const commonConfig = require('./webpack.common.js');

const { root } = require('./helpers');

const { PORT } = process.env;

module.exports = webpackMerge(commonConfig, {
  mode: 'development',
  output: {
    path: root('dist'),
    publicPath: `http://localhost:${PORT}/`,
    filename: '[name].js',
    chunkFilename: '[id].chunk.js',
  },

  optimization: {
    runtimeChunk: false,
    splitChunks: false,
  },

  module: {
    rules: [
      {
        test: /^(?!.*\.test\.tsx?$).*\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
        options: {
          configFile: "tsconfig.dev.json"
        }
      },
    ],
  },

  devServer: {
    historyApiFallback: {
      index: '/',
      disableDotRule: true,
    },
    host: '0.0.0.0',
    port: PORT,
    proxy: {
      '/assets': {
        target: `http://localhost:${PORT}/`,
      },
    },
    stats: 'minimal',
  },
});
