const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const InlineManifestWebpackPlugin = require('inline-manifest-webpack-plugin');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const {root} = require('./helpers');

const config = require('./config');

const htmlConfig = {
  title: config.title,
  themeColor: config.themeColor,
  template: 'src/index.ejs',
  BUILD_NUMBER: process.env.BUILD_NUMBER,
  fav: config.favicon,
};

module.exports = {
  entry: {
    app: './src/index.tsx',
  },

  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    alias: {
      '~': path.join(__dirname, '../src'),
    },
  },

  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader',
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },

  plugins: [
    new webpack.DefinePlugin({
      process: {
        env: {
          ENV: JSON.stringify(process.env.ENV),
          NODE_ENV: JSON.stringify(process.env.NODE_ENV),
          API_BASE_PATH: JSON.stringify(process.env.API_BASE_PATH),
          BUILD_NUMBER: JSON.stringify(process.env.BUILD_NUMBER),
        },
      },
    }),

    new HtmlWebpackPlugin(htmlConfig),

    new CopyWebpackPlugin([{
      from: root('src/assets'),
      to: root('dist/assets'),
    }]),

    new CopyWebpackPlugin([{
      from: root('src/index.ejs'),
      to: root('dist'),
    }]),

    new InlineManifestWebpackPlugin({ name: 'webpackManifest' }),
  ],
};
