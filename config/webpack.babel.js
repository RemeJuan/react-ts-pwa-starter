const webpackMerge = require('webpack-merge');
const commonConfig = require('./webpack.common.js');

const {root} = require('./helpers');

const {PORT} = process.env;

module.exports = webpackMerge(commonConfig, {
  mode: 'development',
  devtool: 'source-map',
  output: {
    path: root('dist'),
    publicPath: '/',
    filename: '[name].js',
    chunkFilename: '[id].chunk.js',
  },

  module: {
    rules: [
      {
        test: /^(?!.*\.test\.tsx?$).*\.tsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
    ],
  },

  optimization: {
    runtimeChunk: false,
    splitChunks: false,
  },

  devServer: {
    historyApiFallback: {
      index: '/',
      disableDotRule: true,
    },
    host: '0.0.0.0',
    port: PORT,
    proxy: {
      '/assets': {
        target: `http://localhost:${PORT}/`,
      },
    },
    stats: 'minimal',
  },
});
